# CLI and The Shell (OBJ 1.1)

The Linux operating system emphasizes a simple user interface in which the users types in text commands into a prompt, and that way, they can interract with the system. In Linux administration, one of the most crucial skill is becoming comfortable at entering text commands using cli.

CLI is a text-based interface between the user and the operating system that accepts input in the form of type commands. In addition to cli, we have something known as the shell. Shell is the user interface used to interract with the operating system which allows the users to pass commands to the kernel.

The original UNIX shell was called sh. It's still available on some Linux systems, it has been virtually replaced by bash. There are other shells like :
- Korn
- Z Shell
- TC Shell

When using shell, its necessarry that make sure of usage of proper syntax. Bash shell syntax contais three main components:
- Command
- Options
- Argument

and there is spaces between each of these. Commands entered in bash are going to be case-sensitive.

```bash
$ command -options [arguments]
# Example
$ touch test.txt # command [argument]
$ cp -i test.txt demo.txt # command -option [argument] [argument]
```

If entering command in the proper syntax failed, bash will return an error.

**Command Not Found :** Check command for typos.
**No Such File or Directory :** Check for typos in the directory, file or file path names.