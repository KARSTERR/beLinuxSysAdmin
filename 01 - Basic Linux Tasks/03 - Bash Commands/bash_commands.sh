#!/bin/bash

echo "Good Morning!" # Prints "Good Morning"
ls -a # List content of the directory including hidden files
pwd # Prints current working directory path
cd .test # Change current directory to .test
cp "../../02 - CLI and The Shell/demo.txt" . # Copy demo.txt file to current directory
clean # Cleans the CLI

################################
#       For Next Command       #
echo "Hello, World!" > demo.txt
#                              #
################################

cat demo.txt # Outputs demo.txt content.

###################################
#        For Next Command         #
for ((i = 1; i <= 100; i++)); do
    echo "Hello, World!" >> demo.txt
done
#                                 #
###################################

less demo.txt # break the content of demo.txt into multiple pages
# Scroll -> Page Up/Down
# Exit -> Q
vi demo.txt
vim demo.txt
nano demo.txt
gedit demo.txt
# shutdown -h now # Shuts down system immediately.
# shutdown -r now # Reboot system immediately.
su 