# Bash Commands

There are several commands that are important to accomplishing basic task within the Linux. Both the command and the syntax are the proper way to enter a command.

**echo :** Repeats input back to the user on the screen.

```bash
$ echo "Good Morning!" # Prints "Good Morning"
```

**ls :** Lists the content of a directory that can give options to view permissions and hidden files.

```bash
$ ls -a # List content of the directory including hidden files
```

**pwd :** Displays the current working directory.

```bash
$ pwd # Prints current working directory path
```

**cd :** Changes the current working directory to another directory.

```bash
$ cd .test # Change directory . to .test
```

**cp :** Copies a file or directory to another location.

```bash
$ cp "../02 - CLI and The Shell/demo.txt" . # Copy demo.txt file to current directory
```

**mkdir :** Creates a new directory.

```bash
$ mkdir newdir # Creates newdir directory.
```

**clear :** Used to clean the cli of all text.

```bash
$ clean # Cleans the CLI
```

**cat :** Used to view the contents of a file without editing options.

```bash
$ cat demo.txt # Outputs demo.txt content.
```

**less :** Used to view the contents of a file that won't fit on one screen.

```bash
$ less demo.txt # break the content of demo.txt into multiple pages
# Scroll -> Page Up/Down
# Exit -> Q
```

**editors :** Used to manipulate text files.
- **vi(m) :** Default text editor for Linux. It's pretty complicated to use for most people.
- **nano :** Simple and user-friendly text editor.
- **gedit :** GUI text editor that requires installation of a desktop environment.

```bash
$ vi demo.txt
$ vim demo.txt
$ nano demo.txt
$ gedit demo.txt
```

**Shutting Down**

```bash
$ shutdown -h now # Shuts down system immediately.
$ shutdown -r now # Reboot system immediately.
```

**su :** Allows to switch user credentials

```bash
$ su # change user to root
```
