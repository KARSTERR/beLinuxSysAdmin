# Linux Design Philosophy (OBJ 1.1)

Linux's philosophy is **opennes** and **transparency**.

Linux is the family of open-source UNIX-like operating systems. Typically every member of the Linux family is called 'Linux Distro'. There are hundreds of Linux distributions in active development like Ubuntu, Debian, Fedora, Arch Linux etc. 

The nature of Linux allows anyone freely download, modify and redistribute it. And it owes this to the fact that it is free and open source.

**Open Source :** Software in which the source code is available for public use. It encourages a collaborative effort among programmers and general users belonging to that software's community.

**Proprietary :** Licensed software that has restrictions on what end users can do.

There are free software licenses like:
- GNU General Public License
- Apache License
- MIT License
- Creative Commons Zero
- ...

Linux follows the UNIX philosophy of simplicity and modularity. This makes Linux more relaible, stable and highly customizable.

But open-source softwares also comes with soem caveats like :
- Steep learning curve
- Not well-supported
- No definitive/official version
- No official vendor-provided support

While all Linux distros are based on the Linux kernel, they do differ primarily in what they offer, as software bundled into that distro. Different distros have differences in the community, the rate of release and other factors to consider too.